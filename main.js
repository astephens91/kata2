function add(a,b) {
    return a + b;
};

console.log("Add Function")
console.log("2 plus 4 is " + add(2,4));

function multiply(c,d) {
    let product = 0;
    for (let i = 0; i < d; i++) {
       product = add(product,c) 
    }
    return product;
};

console.log("Multiply Function");
console.log("6 times 8 is " + multiply(6,8));

function power(e,f) {
    let answer = e;
    for (let i = 1; i < f; i++){
        answer = multiply(answer,e)
        }
    return answer
};

console.log("Power Function");
console.log("2 to the 8th power is " + power(2,8));

function factorial(g) {
    let answer = g;
    for (let i = g - 1; i >= 1; i--) {
        answer = multiply(answer, i)
    }
    return answer;
};

console.log("Factorial Function (Does not work with 0 or 1)");
console.log("4 factorial is " + factorial(4));

function fibonacci(n) {
    let a = 0;
    let b = 1; 
    let fib = 1;
    for(let i = 3; i <= n; i++) {
        fib = a + b;
        a = b;
        b = fib;
    }
    return fib;
};

console.log("Fibonacci Seq");
console.log("The 8th Fibonacci number is " + fibonacci(8));